
```
 _   _                    __ _
| |_| |__  _ __ ___  ___ / _(_)_   _____  ___  _ __   ___
| __| '_ \| '__/ _ \/ _ \ |_| \ \ / / _ \/ _ \| '_ \ / _ \
| |_| | | | | |  __/  __/  _| |\ V /  __/ (_) | | | |  __/
 \__|_| |_|_|  \___|\___|_| |_| \_/ \___|\___/|_| |_|\___|

```

---

## Usage

1. Clone the repo [https://bitbucket.org/manhattanave/threefiveone-starter](https://bitbucket.org/manhattanave/threefiveone-starter).

```bash
git clone git@bitbucket.org:manhattanave/threefiveone-starter.git projectname
```

---

## Development

This project uses up to node 14, sharp has some build issues in v16 at the moment.

```bash
nvm use stable
# Now using node v14.5.0 (npm v6.14.11)
```

### Install

Clone the repo and run

```bash
cd am/assets
npm install
```

### During Development

```bash
npm run dev
```

*Before committing code*:
Run precommit from assets directory before pushing the code for development/contribution.

```
cd assets && npm run precommit
```

---

## Production
This doesn't clone the entire theme, per-se, the way the Gulp based project set up did. It'll just clone the difference and toggle a flag between Dev & Prod.

```bash
npm run prod
```

### Linting & Formatting

The following command will fix most errors and show and remaining ones which cannot be fixed automatically.

```bash
npm run lint:fix
```

We follow the stylelint configuration used in WordPress Gutenberg, run the following command to lint and fix styles.

```bash
npm run stylelint:fix
```

Sometimes you'll have to ignore linting. Here's the documentation:

* CSS Linting https://stylelint.io/user-guide/ignore-code
* ESLint: https://eslint.org/docs/user-guide/configuring/ignoring-code

---

### Order of Doing Things

`npm run dev` to develope things.
`npm run precommit` to this test/build.
then you can safely `git commit -m ...`

---

### Scripts

In `/assets/src/js/main.js`, we now include JS here. Just uncomment what you don't want.

```javascript
// Primary Styles
import '../sass/main.scss';

// FontFaceObserver
var FontFaceObserver = require('fontfaceobserver');

// WebFontLoader
var WebFont = require('webfontloader');

// Magnific Popup
var magnificPopup = require('magnific-popup');

// Slick
var Slick = require('slick-carousel');

// Match Height
var matchHeight = require('jquery-match-height');

```

*Note*: We import our own Slick styles, located at `/assets/src/library/css/slick.css` and `/assets/src/library/css/slick-theme.css`. This is to prevent loading of custom Slick fonts and loader assets. We'll be using our own.

---

## Directory Structure

```php
.
├── README.md
├── assets
│   ├── src ( Where we Edit SASS/CSS/JS/Fonts/Icons/Images )
│   │   └── js
│   │       └── main.js ( Primary Global Javascript )
│   └── webpack.config.js ( Webpack Config )
├── footer.php
├── functions.php ( Global Variables Declared Here )
├── header.php
├── inc
│   ├── classes
│   │   ├── class-threefiveone-theme.php ( Theme Supports )
│   │   ├── class-assets.php ( Script / Styles Enqueues )
│   │   ├── class-blocks.php ( Block Templates )
│   │   ├── class-menus.php ( Menus )
│   │   ├── class-sidebar.php ( Sidebars )
│   ├── helpers
│   │   ├── autoloader.php ( Load /inc code )
│   │   ├── custom-functions.php (351 Custom PHP Functions )
│   │   ├── template-config.php ( Page & Template Creation )
│   │   └── template-tags.php ( Meta functions for Wordpress )
│   └── traits
│       └── trait-singleton.php
├── index.php ( Blog page )
├── page.php  ( Single Page )
├── page-about.php  ( About Page Template )
├── page-home.php  ( Home Page Template )
├── screenshot.png
├── single.php ( Single Post Page )
├── style.css (Just used for Wordpress Theme Info)
└── template-parts
    ├── components
    │   └── blog
    │       ├── entry-content.php
    │       ├── entry-footer.php
    │       ├── entry-header.php
    │       └── entry-meta.php
    ├── content-none.php
    ├── content.php
    └── header
        └── nav.php
```

---

## Fixing Errors

1. Error: Node Sass does not yet support your current environment
Solution :
```shell
cd assets
npm rebuild node-sass
```

---

## Adding Additional NPM Packages
```bash
npm i package-name --save-dev
```

then add the requirement to our main javascript file:
`/assets/src/js/main.js`

like:

```javascript
// Package Name
const packageName = require('package-name');
```

or if the script is not from `node_modules`:

```javascript
// Package Name
const packageName = require('../library/path/to/file.js');
```

---

## Local Fonts
Localized fonts can live in `/assets/src/library/fonts/`.

---

## Local Scripts
Localized, non-npm scripts can live in `/assets/src/library/js/`. You'll see this is where InstagramFeed.js is.

---

## Permalinks
In `/inc/helpers/template-config.php` we programmatically set the permalink structure.

To forcefully refresh via command line and wp-cli:

```
wp rewrite flush
```

---

## SASS Variables
Everything variables wise is located in `am/assets/src/sass/settings`


---


### CSS Variables + Customization

In `/assets/src/sass/settings/_foundation-settings.scss` you can edit SASS variables, including fonts, grid margins, etc. These will tie into Foundation. Custom variables and 'aliases' live here: `/assets/src/sass/settings/_variables.scss`.

### New Media Query Mixins

Foundation has its own better mixins for responsive breakpoints. I've excluded the previous custom mixins based off default breakpoints from this project. Examples can be viewed here: https://get.foundation/sites/docs/media-queries.html#the-breakpoint-mixin.

```scss
.element {
  // Only affects medium screens and smaller
  @include breakpoint(medium down) { }
  // Only affects medium screens, not small or large
  @include breakpoint(medium only) { }
}
```

We can override the breakpoints in `_foundation-settings`:

```scss
$breakpoints: (
  small: 0,
  medium: 640px,
  large: 1024px,
  xlarge: 1200px,
  xxlarge: 1440px,
);
```

---

### Grid Margin/Padding

So before we had a variable, `$grid__margin` for unifying grid margin/paddings. Now we can use Foundation's native mixin for this. More details available here: https://get.foundation/sites/docs/xy-grid.html#gutters

```scss
div {
  @include xy-gutters($gutter-position: left right bottom);
}
```

The mixin is located at: `/assets/node_modules/foundation-sites/scss/xy-grid/_gutters.scss`.

---

### Linting

Javascript functions with returns or variables have to have comments before them:

The Comment before would be:
```js
/**
 * Title Goes Here
 *
 * @param {object} element Description Goes Here
 * @return {object} Returns the Child Image.
 */
test: function(element) {
  return element.find('img');
}
```

You can read more about that here: https://eslint.org/docs/2.0.0/rules/valid-jsdoc

If the function doesn't return anything or have params, the following will suffice:

```js
/**
 * Init
 */
```

---

### Favicons

Favicons are generated from the SVG located in `/assets/src/favicon-source`. They are generated when you run `npm run prod` and live in the `build` directory. This should be an SVG, but can be changed in the `logo` line in `webpack.config.js`:

```js
  new FaviconsWebpackPlugin( {
    logo: './src/favicons-source/logo.svg',
    cache: true,
    prefix: '/favicons/',
    devMode: 'light',
    inject: false,
    favicons: {
      appName: '351Studios',
      appDescription: 'Website for 351Studios',
      developerName: '351 Studios',
      developerURL: 'https://351studios.com',
      background: '#fff',
      theme_color: '#333',
      lang: 'en-US',
    }
  } ),
    ...
```

Options under `favicons` should be changed to whatever the works best for the client. If using SVG or PNG as the source, if there's any transparencies, that'll carry over to the favicon. You may want to put a background for dark/light mode differences.

---

### Modals

We're usinig magnific for images, may as well use it for modals too; they got some really cool features and animations. Peep `/template-parts/modals/modal.php` for all the examples. I pulled this from their official [documentation](https://codepen.io/dimsemenov/pen/GAIkt). Overrides are in `main.scss`.

---

### Custom PHP Functions

Added some new PHP functions to make life easier, most reside in `inc/helpers/custom-functions.php`

Examples:

```php
// Images
function am_get_image($image) {
    $dir = get_template_directory() . '/assets/build/img/' . $image;
    $uri = get_template_directory_uri() . '/assets/build/img/' . $image;
    if (file_exists($dir)) {
        return $uri;
    }
    return false;
}

// Fancy Debug
function am_dump($var, $name = null) {
    echo "<div style='padding:10px;background:rgba(0,0,0,.1);position:relative;'>";
    if ( $name ) {
        echo "<div style='font-weight:bold;padding:20px;border: 1px dashed #aaa;'>" . $name . "</div>";
    }
    echo "<xmp style='max-height: 400px;overflow-y: scroll;padding: 20px;font-size: 12px;border: 1px dashed #aaa;margin-bottom: 20px;'>";

    // Output
    if ( is_array($var) ) {
        print_r($var);
    } else if ( gettype($var) == "object" ) {
        print_r($var);
    } else if ( $var === null ) {
        echo "NULL";
    } else if ( is_bool($var) ) {
        if ($var) {
            echo "TRUE";
    } else {
            echo "FALSE";
        }
    }  else {
        print_r($var);
    }
    echo "</xmp>";
    echo "</div>";
}
```
