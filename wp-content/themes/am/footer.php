<?php
/**
 * Footer template
 *
 * @package ThreeFiveOne
 */
?>


<footer class="footer">
	<div class="grid-container full">
		<div class="grid-x">
			<div class="cell medium-4">
				<ul class="footer__links">
					<li class="footer__link"><a href="#">Contact</a></li>
					<!-- <li class="footer__link"><a href="#">Privacy Policy</a></li> -->
					<!-- <li class="footer__link"><a href="#">Cookie Policy</a></li> -->
				</ul>
			</div>
			<div class="cell medium-5 medium-offset-3 footer__social-links">
				<span><a class="footer__social-link" href="https://www.instagram.com/marshmallowlaserfeast/?hl=en">Instagram</a>, <a class="footer__social-link" href="https://www.facebook.com/marshmallowlaserfeast">Facebook</a>, <a class="footer__social-link" href="#">Twitter</a>, <a class="footer__social-link" href="https://www.linkedin.com/company/marshmallow-laser-feast-limited/">LinkedIn</a>, <a class="footer__social-link" href="https://vimeo.com/marshmallowlaserfeast">Vimeo</a>, <a class="footer__social-link" href="https://www.youtube.com/user/marshmallowlaservids?app=desktop">Youtube</a></span>
			</div>
		</div>
	</div>
</footer>

</div>
</div>

<?php wp_footer(); ?>

<?php
if ( class_exists('ACF') ) {
	$google_analytics_key = !empty(get_field('google_analytics_key', 'option')) ? get_field('google_analytics_key', 'option') : false;
}

if ($google_analytics_key): ?>
<script>
(function(f,i,r,e,s,h,l){i['GoogleAnalyticsObject']=s;f[s]=f[s]||function(){
(f[s].q=f[s].q||[]).push(arguments)},f[s].l=1*new Date();h=i.createElement(r),
l=i.getElementsByTagName(r)[0];h.async=1;h.src=e;l.parentNode.insertBefore(h,l)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', '<?php echo $google_analytics_key; ?>', '<?php echo $_SERVER['HTTP_HOST']; ?>');
ga('send', 'pageview');
</script>
<?php endif; ?>

</body>
</html>
