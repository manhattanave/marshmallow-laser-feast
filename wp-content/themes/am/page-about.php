<?php
/**
 * Template Name: About
 *
 * @package ThreeFiveOne
 */

get_header();

if ( !post_password_required() ):
?>

<main id="main" class="site-main" role="main">
	<?php
	// Page Content
	get_template_part('template-parts/single/about', 'page');
	?>
</main>

<?php
// Admin Edit
get_template_part('template-parts/admin', 'edit');

// Password Form
else:
	get_template_part('template-parts/password', 'form');
endif;

get_footer();
