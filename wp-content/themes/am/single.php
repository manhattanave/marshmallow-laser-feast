<?php
/**
 * Single post template file.
 *
 * @package ThreeFiveOne
 */

get_header();

if ( !post_password_required() ):
?>

	<main id="main" class="site-main mt-5" role="main">
		<div class="grid-container">
			<div class="grid-x">
				<div class="cell">
					<?php
					if ( have_posts() ) :
						while ( have_posts() ) : the_post();
							get_template_part( 'template-parts/content' );
						endwhile;
					else :
						get_template_part( 'template-parts/content-none' );
					endif;
					?>
				</div>
				<?php
				// Next and previous link for page navigation.
				?>
				<div class="prev-link"><?php previous_post_link(); ?></div>
				<div class="next-link"><?php next_post_link(); ?></div>
			</div>
		</div>
	</main>

<?php
// Admin Edit
get_template_part('template-parts/admin', 'edit');

// Password Form
else:
	get_template_part('template-parts/password', 'form');
endif;

get_footer();
?>
