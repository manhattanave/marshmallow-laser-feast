<?php
/**
 * Main template file.
 *
 * @package ThreeFiveOne
 */

get_header();
?>

<main id="main" class="site-main" role="main">
	<section class="archive-heading">
		<?php if (false): ?>
		<div class="grid-container">
			<div class="grid-x grid-padding-x">
				<div class="cell small-12">
					<h1 class="archive-heading__heading">Projects</h1>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php get_template_part( 'template-parts/header/subcat', 'mobile' ); ?>
		<div class="grid-container project-tiles">
			<div id="projects-grid" class="grid-x grid-padding-x project-tiles-inner-wrapper isotope">
			<?php if (have_posts()):
				$x = 0;
				while (have_posts()) : the_post();
					$project_info = get_field('project_info');
					$project_info = !empty($project_info) ? $project_info : false;

					$project_name = !empty($project_info['project_name']) ? $project_info['project_name'] : get_the_title();
					$project_category_text = !empty($project_info['project_category_text']) ? $project_info['project_category_text'] : '';
					$project_year = !empty($project_info['project_year']) ? $project_info['project_year'] : '';

					$project_thumbnail_landscape = !empty(get_field('project_images')['project_thumbnail_landscape']) ? get_field('project_images')['project_thumbnail_landscape']['url'] : get_the_post_thumbnail_url();
					$project_thumbnail_portrait = !empty(get_field('project_images')['project_thumbnail_portrait']) ? get_field('project_images')['project_thumbnail_portrait']['url'] : get_the_post_thumbnail_url();

					$video_landscape_url = !empty(get_field('project_images')['project_thumbnail_landscape_video']) ? get_field('project_images')['project_thumbnail_landscape_video'] : false;

					$video_portrait_url = !empty(get_field('project_images')['project_thumbnail_portrait_video']) ? get_field('project_images')['project_thumbnail_portrait_video'] : false;					

					$proj_img = ($x % 2 == 0) ? $project_thumbnail_landscape : $project_thumbnail_portrait;
					$proj_video = ($x % 2 == 0) ? $video_landscape_url : $video_portrait_url;

					$lwidth = !empty(get_field('project_images')['project_thumbnail_landscape']['width']) ? get_field('project_images')['project_thumbnail_landscape']['width'] : false;
					$lheight = !empty(get_field('project_images')['project_thumbnail_landscape']['height']) ? get_field('project_images')['project_thumbnail_landscape']['height'] : false;

					$pwidth = !empty(get_field('project_images')['project_thumbnail_portrait']['width']) ? get_field('project_images')['project_thumbnail_portrait']['width'] : false;
					$pheight = !empty(get_field('project_images')['project_thumbnail_portrait']['height']) ? get_field('project_images')['project_thumbnail_portrait']['height'] : false;	

					$project_aspect_ratio = '';
					$project_aspect_ratio = ($x % 2 == 0) ? 'project-tile__horizontal' : 'project-tile__vertical';
					
					$vwidth = ($x % 2 == 0) ? $lwidth : $pwidth;
					$vheight = ($x % 2 == 0) ? $lheight : $pheight;

					$topic_term = get_the_terms(get_the_ID(), 'topics');
					$topic_term_select = !empty($topic_term) ? $topic_term[0]->slug : '';

					$cats = get_the_category();
					$cats = !empty($cats) ? $cats : false;
					$cat_class = '';
					if ($cats) :
						foreach ($cats as $cat):
							$cat_class .= $cat->slug . ' ';
						endforeach;
					endif;
				?>
					<div class="cell small-12 medium-6 large-4 isotope-item project-tile <?php echo $topic_term_select; ?> <?php echo $cat_class; ?>">
						<a href="<?php echo get_the_permalink(); ?>">
							<div class="project-tile__inner <?php echo $project_aspect_ratio; ?>">
								<?php if (!$video_landscape_url && !$video_portrait_url): ?>
									<div class="project-tile__image-container">

										<div class="project-tile__image-inner">
											<?php if ($project_aspect_ratio == 'project-tile__horizontal'): ?>
												<div class="sixteenbynine">
											<?php endif; ?>
											<img src="<?php echo $proj_img; ?>" class="project-tile__image content">
											<?php if ($project_aspect_ratio == 'project-tile__horizontal'): ?>
												</div>
											<?php endif; ?>
										</div>
										
									</div>
								<?php else: ?>
									<div class="project-tile__image-container project-tile__image-container--video">
										<div class="project-tile__video-inner" style="padding-top: <?php echo ($vheight / $vwidth) * 100; ?>%">
											<video class="project-tile__video" autoplay loop muted playsinline src="<?php echo $proj_video; ?>" poster="<?php echo $proj_img; ?>"></video>
										</div>
									</div>
								<?php endif; ?>
								<h2 class="project-tile__heading"><?php echo get_the_title(); ?></h2>
								<div class="project-tile__info">
									<span class="project-tile__category"><?php echo $project_category_text; ?></span>
									<span class="project-tile__date"><?php echo $project_year; ?></span>
								</div>
							</div>
						</a>
					</div>
				<?php $x++;
				endwhile;
				?>
				</div>
			<?php endif; ?>
			</div>
		</div>
	</section>
</main>

<?php
get_footer();
?>
