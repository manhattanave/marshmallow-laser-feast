<?php
/**
 * Single project template file.
 *
 * @package ThreeFiveOne
 */

get_header(); ?>
<?php
if ( !post_password_required() ):
?>

	<main id="main" class="site-main mt-5" role="main">

			<?php
			if ( have_posts() ) :
				while ( have_posts() ) : the_post();
					get_template_part( 'template-parts/single/project' );
				endwhile;
			else :
				get_template_part( 'template-parts/content-none' );
			endif;
			?>
		</div>
		<?php
		// Next and previous link for page navigation.
		?>
	</main>

<?php
// Admin Edit
get_template_part('template-parts/admin', 'edit');

// Password Form
else:
	get_template_part('template-parts/password', 'form');
endif;

get_footer();
?>
