<?php
/**
 * 404 Page
 *
 * @package ThreeFiveOne
 */

get_header();
?>

<main id="main" class="site-main" role="main">
	<?php
	// Page Content
	get_template_part('template-parts/single/content-none');
	?>
</main>

<?php
get_footer();
