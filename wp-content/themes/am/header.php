<?php
/**
 * Header template.
 *
 * @package ThreeFiveOne
 */

if ( class_exists('ACF') ) {
	$google_tag_manager = !empty(get_field('google_tag_manager_key', 'option')) ? get_field('google_tag_manager_key', 'option') : false;
}
?>
<!doctype html>
<html <?php language_attributes(); ?> data-version="<?php echo THREEFIVEONE_VERSION_NUMBER; ?>">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/favicon.ico'; ?>">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/favicon-16x16.png'; ?>">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/favicon-32x32.png'; ?>">
	<link rel="icon" type="image/png" sizes="48x48" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/favicon-48x48.png'; ?>">
	<link rel="manifest" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/manifest.json'; ?>">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="#333">
	<meta name="application-name" content="351Studios">
	<link rel="apple-touch-icon" sizes="57x57" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-57x57.png'; ?>">
	<link rel="apple-touch-icon" sizes="60x60" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-60x60.png'; ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-72x72.png'; ?>">
	<link rel="apple-touch-icon" sizes="76x76" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-76x76.png'; ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-114x114.png'; ?>">
	<link rel="apple-touch-icon" sizes="120x120" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-120x120.png'; ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-144x144.png'; ?>">
	<link rel="apple-touch-icon" sizes="152x152" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-152x152.png'; ?>">
	<link rel="apple-touch-icon" sizes="167x167" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-167x167.png'; ?>">
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-180x180.png'; ?>">
	<link rel="apple-touch-icon" sizes="1024x1024" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-icon-1024x1024.png'; ?>">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
	<meta name="apple-mobile-web-app-title" content="351Studios">
	<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-640x1136.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-750x1334.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-828x1792.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1125x2436.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1242x2208.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1242x2688.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1536x2048.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1668x2224.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1668x2388.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2048x2732.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 810px) and (device-height: 1080px) and (-webkit-device-pixel-ratio: 2) and (orientation: portrait)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1620x2160.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 320px) and (device-height: 568px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1136x640.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 667px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1334x750.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-1792x828.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 375px) and (device-height: 812px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2436x1125.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 736px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2208x1242.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 414px) and (device-height: 896px) and (-webkit-device-pixel-ratio: 3) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2688x1242.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 768px) and (device-height: 1024px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2048x1536.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1112px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2224x1668.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 834px) and (device-height: 1194px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2388x1668.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 1024px) and (device-height: 1366px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2732x2048.png'; ?>">
	<link rel="apple-touch-startup-image" media="(device-width: 810px) and (device-height: 1080px) and (-webkit-device-pixel-ratio: 2) and (orientation: landscape)" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/apple-touch-startup-image-2160x1620.png'; ?>">
	<link rel="icon" type="image/png" sizes="228x228" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/coast-228x228.png'; ?>">
	<meta name="msapplication-TileColor" content="#fff">
	<meta name="msapplication-TileImage" content="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/mstile-144x144.png'; ?>">
	<meta name="msapplication-config" content="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/browserconfig.xml'; ?>">
	<link rel="yandex-tableau-widget" href="<?php echo get_template_directory_uri() . '/assets/build/icons/favicons/yandex-browser-manifest.json'; ?>">
	<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />

	<?php wp_head(); ?>

	<?php if ($google_tag_manager): ?>
	<script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $google_tag_manager; ?>"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', '<?php echo $google_tag_manager; ?>');
	</script>
	<?php endif; ?>
</head>
<body <?php body_class(); ?>>

<?php
if ( function_exists( 'wp_body_open' ) ) {
	wp_body_open();
}
?>

<div id="page" class="site">
	<header id="masthead" class="header" role="banner">
		<div class="header__logo-wrapper">
			<button id="mobile-menu-button" class="reset anim-menu-btn js-anim-menu-btn" aria-label="Toggle menu">
			  <i class="anim-menu-btn__icon anim-menu-btn__icon--close" aria-hidden="true"></i>
			</button>
			<div class="logo">
				<a href="<?php echo esc_url( home_url() ); ?>">
					<?php if (is_page('home')): ?>
						<img src="<?php echo esc_url( am_get_image('mlf-logo-white.png') ); ?>" alt="Logo" class="logo-img hide-for-small-only">
						<img src="<?php echo esc_url( am_get_image('mlf-logo.png') ); ?>" alt="Logo" class="logo-img show-for-small-only">
					<?php else: ?>
						<img src="<?php echo esc_url( am_get_image('mlf-logo.png') ); ?>" alt="Logo" class="logo-img">
					<?php endif; ?>						
				</a>
			</div>
		</div>
		<nav class="nav" role="navigation">
			<?php get_template_part( 'template-parts/header/nav' ); ?>
		</nav>
	</header>
	<div id="content" class="site-content">

