<?php
/**
 * Main template file.
 *
 * @package ThreeFiveOne
 */

get_header();
?>

	<main id="main" class="site-main" role="main">
		<section class="archive-heading">
<!-- 			<div class="grid-container">
				<div class="grid-x grid-padding-x">
					<div class="cell small-12">
						<h1 class="archive-heading__heading">Exhibitions</h1>						
					</div>
				</div>
			</div> -->
		
			<?php get_template_part( 'template-parts/header/yearmenu', 'mobile' ); ?>			
			<div class="grid-container exhibition-tiles">
				<div id="exhibitions-grid" class="exhibition-tiles-inner-wrapper grid-x grid-padding-x isotope">
				<?php 
				$dedupe = array();
				$feat_args = array(
					'post_type' => 'exhibitions',
					'post_status' => 'publish',
					'meta_key' => 'exhibition_active',
					'meta_value' => 1,
					'post_count' => 2
				);
				$feat_query = new WP_Query($feat_args); ?>	
				<?php if ($feat_query->have_posts()): 
					$i = 1;
					while ($feat_query->have_posts()) : $feat_query->the_post();
						$theID = get_the_ID();
						$exhibition_info = get_field('exhibition_info', $theID);
						$exhibition_info = !empty($exhibition_info) ? $exhibition_info : false;

						$exhibition_name = !empty($exhibition_info['exhibition_name']) ? $exhibition_info['exhibition_name'] : get_the_title($theID);
						$exhibition_venue = !empty($exhibition_info['exhibition_venue']) ? $exhibition_info['exhibition_venue'] : '';
						$exhibition_location = !empty($exhibition_info['exhibition_location']) ? $exhibition_info['exhibition_location'] : '';
						$exhibition_date = !empty($exhibition_info['exhibition_date']) ? $exhibition_info['exhibition_date'] : '';
						$date=date_create($exhibition_date);

						$exhibition_end_date = !empty($exhibition_info['exhibition_end_date']) ? $exhibition_info['exhibition_end_date'] : '';
						$exhibition_time = !empty($exhibition_info['exhibition_time']) ? $exhibition_info['exhibition_time'] : '';
						$exhibition_timezone = !empty($exhibition_info['exhibition_timezone']) ? $exhibition_info['exhibition_timezone'] : '';
						$exhibition_link = get_field('exhibition_link', $theID);
						$exhibition_link_name = get_field('exhibition_link_name', $theID);
						$project_url = get_field('exhibition_project');
						$project_url = !empty($project_url) ? get_the_permalink($project_url->ID) : '#'; 
						$reverse = $i == 2 ? 'reverse' : '';
						array_push($dedupe, get_the_ID());

						$cats = get_the_category();
						$cats = !empty($cats) ? $cats : false;
						$cat_class = '';
						if ($cats) :
							foreach ($cats as $cat):
								$cat_class .= $cat->slug . ' ';
							endforeach;
						endif;						
					 ?>


					<?php if ($i == 1 || $i == 2): ?>
					<div class="grid-sizer cell medium-3"></div>
					<div class="cell small-12 isotope-item <?php echo date_format($date,"Y"); ?> <?php echo $cat_class; ?>">
						<div class="grid-x grid-padding-x exhibition-tile <?php echo $reverse; ?>">
							<div class="cell small-12 medium-7">
								<img src="<?php echo get_the_post_thumbnail_url() ?>" class="exhibition-tile__image">
							</div>
							<div class="cell small-12 medium-5">
								<h2 class="exhibition-tile__heading"><a href="<?php echo $project_url; ?>"><?php echo get_the_title(); ?></a></h2>
								<span class="exhibition-tile__venue"><?php echo $exhibition_venue; ?></span>

								<div class="exhibition-tile__info">
									<span class="exhibition-tile__date"><?php echo date_format($date,"l d F Y"); ?></span>
									<span class="exhibition-tile__location">Location: <?php echo $exhibition_location; ?></span>
								</div>

								<div class="exhibition-tile__content">
									<p><?php echo get_the_excerpt(); ?></p>
								</div>

								<div class="exhibition-tile__link">
									<a href="<?php echo $exhibition_link; ?>"><?php echo $exhibition_link_name; ?></a>
								</div>
							</div>
						</div>
					</div>						
					<?php endif; ?>
					<?php $i++; ?>
				<?php endwhile; ?>
				<?php endif; ?>
			
				<?php 
				$main_args = array(
					'post_type' => 'exhibitions',
					'post_status' => 'publish',
					'post_count' => -1,
					'post__not_in' => $dedupe
				);
				$main_query = new WP_Query($main_args); ?>
				<?php if ($main_query->have_posts()): ?>

					<?php 
					while ($main_query->have_posts()) : $main_query->the_post();
					$theID = get_the_ID();
					$exhibition_info = get_field('exhibition_info', $theID);
					$exhibition_info = !empty($exhibition_info) ? $exhibition_info : false;

					$exhibition_name = !empty($exhibition_info['exhibition_name']) ? $exhibition_info['exhibition_name'] : get_the_title($theID);
					$exhibition_venue = !empty($exhibition_info['exhibition_venue']) ? $exhibition_info['exhibition_venue'] : '';
					$exhibition_location = !empty($exhibition_info['exhibition_location']) ? $exhibition_info['exhibition_location'] : '';
					$exhibition_date = !empty($exhibition_info['exhibition_date']) ? $exhibition_info['exhibition_date'] : '';
					$date=date_create($exhibition_date);

					$exhibition_end_date = !empty($exhibition_info['exhibition_end_date']) ? $exhibition_info['exhibition_end_date'] : '';
					$exhibition_time = !empty($exhibition_info['exhibition_time']) ? $exhibition_info['exhibition_time'] : '';
					$exhibition_timezone = !empty($exhibition_info['exhibition_timezone']) ? $exhibition_info['exhibition_timezone'] : '';
					$exhibition_link = get_field('exhibition_link', $theID);
					$exhibition_link_name = get_field('exhibition_link_name', $theID);
					$project_url = get_field('exhibition_project');
					$project_url = !empty($project_url) ? get_the_permalink($project_url->ID) : '#'; 
					$reverse = $i == 2 ? 'reverse' : ''; ?>					

					<div class="cell small-12 medium-3 isotope-item <?php echo date_format($date,"Y"); ?>">
						<div class="exhibition-tile-small util__equal">						
							<img src="<?php echo get_the_post_thumbnail_url() ?>" class="exhibition-tile__image">
							<h2 class="exhibition-tile__heading"><a href="<?php echo $project_url; ?>"><?php echo get_the_title(); ?></a></h2>
							<span class="exhibition-tile__venue"><?php echo $exhibition_venue; ?></span>

							<div class="exhibition-tile__info">
								<span class="exhibition-tile__date"><?php echo date_format($date,"l d F Y"); ?></span>
								<span class="exhibition-tile__location">Location: <?php echo $exhibition_location; ?></span>
							</div>

							<div class="exhibition-tile__content">
								<p><?php echo get_the_excerpt(); ?></p>
							</div>

							<div class="exhibition-tile__link">
								<a href="<?php echo $exhibition_link; ?>"><?php echo $exhibition_link_name; ?></a>
							</div>
						</div>
					</div>					
					
				<?php endwhile; ?>
				<?php endif; ?>
				
				</div>
			</div>
		</section>
	</main>

<?php
get_footer();
?>
