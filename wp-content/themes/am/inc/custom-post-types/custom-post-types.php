<?php
/**
 * Custom Post Types
 *
 * @package ThreeFiveOne
 */

function am_custom_post_types() {

    // Projects
    $labels_projects = array(
        'name'                => _x( 'Projects', 'Post Type General Name', 'am' ),
        'singular_name'       => _x( 'Project', 'Post Type Singular Name', 'am' ),
        'menu_name'           => __( 'Projects', 'am' ),
        'parent_item_colon'   => __( 'Parent Project', 'am' ),
        'all_items'           => __( 'All Projects', 'am' ),
        'view_item'           => __( 'View Project', 'am' ),
        'add_new_item'        => __( 'Add New Project', 'am' ),
        'add_new'             => __( 'Add New', 'am' ),
        'edit_item'           => __( 'Edit Project', 'am' ),
        'update_item'         => __( 'Update Project', 'am' ),
        'search_items'        => __( 'Search Projects', 'am' ),
        'not_found'           => __( 'Projects Not Found', 'am' ),
        'not_found_in_trash'  => __( 'Projects not found in Trash', 'am' ),
    );
    $args_projects = array(
        'label'               => __( 'Projects', 'am' ),
        'description'         => __( 'Projects: A library of MLF projects.', 'am' ),
        'labels'              => $labels_projects,
        'menu_icon'           => 'dashicons-tide',
        'supports'            => array( 'thumbnail', 'editor', 'title', 'revisions' ),
        'taxonomies'          => array( 'category' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 110.4,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
        'show_in_rest'     => true,
    );

    // Exhibitions
    $labels_exhibitions = array(
        'name'                => _x( 'Exhibitions', 'Post Type General Name', 'am' ),
        'singular_name'       => _x( 'Exhibition', 'Post Type Singular Name', 'am' ),
        'menu_name'           => __( 'Exhibitions', 'am' ),
        'parent_item_colon'   => __( 'Parent Exhibition', 'am' ),
        'all_items'           => __( 'All Exhibitions', 'am' ),
        'view_item'           => __( 'View Exhibition', 'am' ),
        'add_new_item'        => __( 'Add New Exhibition', 'am' ),
        'add_new'             => __( 'Add New', 'am' ),
        'edit_item'           => __( 'Edit Exhibition', 'am' ),
        'update_item'         => __( 'Update Exhibition', 'am' ),
        'search_items'        => __( 'Search Exhibitions', 'am' ),
        'not_found'           => __( 'No Exhibitions Found', 'am' ),
        'not_found_in_trash'  => __( 'No Exhibitions Found in Trash', 'am' ),
    );
    $args_exhibitions = array(
        'label'               => __( 'Exhibitions', 'am' ),
        'description'         => __( 'Exhibitions: A collection of all Job listings.', 'am' ),
        'labels'              => $labels_exhibitions,
        'menu_icon'           => 'dashicons-calendar',
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' ),
        'taxonomies'          => array( 'category' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 110.4,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    // Videos
    $labels_videos = array(
        'name'                => _x( 'Videos', 'Post Type General Name', 'am' ),
        'singular_name'       => _x( 'Video', 'Post Type Singular Name', 'am' ),
        'menu_name'           => __( 'Videos', 'am' ),
        'parent_item_colon'   => __( 'Parent Video', 'am' ),
        'all_items'           => __( 'All Videos', 'am' ),
        'view_item'           => __( 'View Video', 'am' ),
        'add_new_item'        => __( 'Add New Video', 'am' ),
        'add_new'             => __( 'Add New', 'am' ),
        'edit_item'           => __( 'Edit Video', 'am' ),
        'update_item'         => __( 'Update Video', 'am' ),
        'search_items'        => __( 'Search Videos', 'am' ),
        'not_found'           => __( 'No Videos Found', 'am' ),
        'not_found_in_trash'  => __( 'No Videos Found in Trash', 'am' ),
    );
    $args_videos = array(
        'label'               => __( 'Videos', 'am' ),
        'description'         => __( 'Videos: A collection of all videos for X.', 'am' ),
        'labels'              => $labels_videos,
        'menu_icon'           => 'dashicons-video-alt2',
        'supports'            => array( 'title', 'editor', 'thumbnail', 'revisions' ),
        'taxonomies'          => array( 'category' ),
        'hierarchical'        => false,
        'public'              => true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 110.4,
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => true,
        'capability_type'     => 'page',
    );

    register_post_type( 'projects', $args_projects );
    register_post_type( 'exhibitions', $args_exhibitions );
    // register_post_type( 'videos', $args_videos );

}

// Hook it in
add_action( 'init', 'am_custom_post_types', 0 );
