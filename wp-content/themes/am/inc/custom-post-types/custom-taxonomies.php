<?php
/**
 * Custom Taxonomies
 *
 * @package ThreeFiveOne
 */

function am_custom_taxonomy() {

    $labels = array(
        'name' => _x( 'Topics', 'taxonomy general name' ),
        'singular_name' => _x( 'Topic', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Topics' ),
        'all_items' => __( 'All Topics' ),
        'parent_item' => __( 'Parent Topic' ),
        'parent_item_colon' => __( 'Parent Topic:' ),
        'edit_item' => __( 'Edit Topic' ),
        'update_item' => __( 'Update Topic' ),
        'add_new_item' => __( 'Add New Topic' ),
        'new_item_name' => __( 'New Topic Name' ),
        'menu_name' => __( 'Topics' )
    );

    register_taxonomy('topics', '', array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'topics' ),
        'show_in_rest' => true,
        'with_front' => true,
        'public' => true,
    ));

    register_taxonomy_for_object_type( 'topics', 'projects' );

}

add_action( 'init', 'am_custom_taxonomy', 0 );
