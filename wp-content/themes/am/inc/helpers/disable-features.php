<?php
/**
 * Disabled Features
 *
 * @package ThreeFiveOne
 */

/**
 * Prevent Access to Comments
 *
 * @return void
 */
add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;

    if ($pagenow === 'edit-comments.php') {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal');

    // Disable support for comments and trackbacks in post types
    foreach (get_post_types() as $post_type) {
        if (post_type_supports($post_type, 'comments')) {
            remove_post_type_support($post_type, 'comments');
            remove_post_type_support($post_type, 'trackbacks');
        }
    }
});

/**
 * Close comments on the front-end
 *
 * @return void
 */
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

/**
 * Remove RSS Links to Comments
 *
 * @return void
 */
function threefiveone_head_cleanup(){
    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );
    // disable comments feed
    add_filter( 'feed_links_show_comments_feed', '__return_false' );
}
add_action( 'after_setup_theme', 'threefiveone_head_cleanup' );

/**
 * Hide existing comments
 *
 * @return void
 */
add_filter('comments_array', '__return_empty_array', 10, 2);

/**
 * Remove comments page in menu
 *
 * @return void
 */
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

/**
 * Remove comments links from admin bar
 *
 * @return void
 */
add_action('init', function () {
    if (is_admin_bar_showing()) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

/**
 * Remove Admin bar
 *
 * @return false
 */
function threefiveone_remove_admin_bar() {
    return false;
}
add_filter( 'show_admin_bar', 'threefiveone_remove_admin_bar' );

/**
 * Remove RSD Links
 *
 * @return void
 */
remove_action( 'wp_head', 'rsd_link' ) ;

/**
 * Remove Emojis
 *
 * @return void
 */
remove_action( 'wp_head', 'print_emoji_detection_script', 7);
remove_action( 'wp_print_styles', 'print_emoji_styles');
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
remove_action( 'admin_print_styles', 'print_emoji_styles' );

/**
 * Remove Shortlinks
 *
 * @return false
 */
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

/**
 * Remove Shortlinks
 *
 * @return void
 */
function threefiveone_disable_heartbeat() {
    if (!is_admin()) {
        wp_deregister_script('heartbeat');
    }
}
add_action( 'init', 'threefiveone_disable_heartbeat', 1 );

/**
 * Disable Dashicons
 *
 * @return true
 */
function wpdocs_dequeue_dashicon() {
    if (is_admin()) {
        return;
    }
    wp_deregister_style('dashicons');
}
add_action( 'wp_enqueue_scripts', 'wpdocs_dequeue_dashicon' );

/**
 * Disable Pingbacks
 *
 * @return void
 */
function disable_pingback( &$links ) {
    foreach ( $links as $l => $link ) {
        if ( 0 === strpos( $link, get_option( 'home' ) ) ) {
            unset($links[$l]);
        }
    }
}
add_action( 'pre_ping', 'disable_pingback' );

/**
 * Disable WLManifest Link
 *
 * @return void
 */
remove_action( 'wp_head', 'wlwmanifest_link' ) ;

/**
 * Disable XML RPC WordPress API
 *
 * @return void
 */
add_filter('xmlrpc_enabled', '__return_false');

/**
 * Disable Auto Updates
 *
 * @return void
 */
define( 'WP_AUTO_UPDATE_CORE', false );
add_filter( 'auto_update_theme', '__return_false' );
add_filter( 'auto_update_plugin', '__return_false' );

/**
 * Disable Access to API
 *
 * @return string
 */
add_filter( 'rest_authentication_errors', function( $result ) {

    $quotes = array(
        'Med Amine Khelifi' => 'Human Stupidity, that’s why Hackers always win.',
        'Ram Dass' => 'The quieter you become, the more you are able to hear…',
        'Roger H. Lincoln' => 'Never tell everything you know…',
        'Cory Doctorow' => 'Never underestimate the determination of a kid who is time-rich and cash-poor.',
        'Paul Graham' => 'There are few sources of energy so powerful as a procrastinating college student.',
        'Mike Tyson' => 'Everybody has a plan until they get punched in the mouth.',
        'a Brahmin to Onesicritus, 327 BC, reported in Strabo\'s Geography' => 'The condition of man is already close to satiety and arrogance, and there is danger of destruction of everything in existence.',
        'Jan Houtema' => 'Change breaks the brittle.',
        'Richard Feynman' => 'The imagination of nature is far, far greater than the imagination of man.',
        'E. B. White' => 'The best writing is rewriting.',
        'Donald Knuth' => 'Premature optimization is the root of all evil (or at least most of it) in programming.',
        'Steve Jobs' => 'Focusing is about saying no.',
        'Winston Churchill' => 'However beautiful the strategy, you should occasionally look at the result.',
        'George Eliot' => 'The important work of moving the world forward does not wait to be done by perfect men.',
        'Tim O\'Reilly' => 'Work on stuff that matters.',
        'Teller' => 'Sometimes magic is just someone spending more time on something than anyone else might reasonably expect.',
        'divorce complaint of Richard Feynman\'s second wife' => 'He begins working calculus problems in his head as soon as he awakens. He did calculus while driving in his car, while sitting in the living room, and while lying in bed at night.',
        'Bob Klein, chief engineer of the F-14 program' => 'The best way to do something \'lean\' is to gather a tight group of people, give them very little money, and very little time.',
        'Abelson & Sussman, SICP, preface to the first edition' => 'Programs must be written for people to read, and only incidentally for machines to execute.'
    );

    $person = array_rand( $quotes );

    if ( ! empty( $result ) ) {
        return $result;
    }

    if ( ! is_user_logged_in() ) {
        return new WP_Error(
            rest_authorization_required_code(),
            get_bloginfo( 'name' ),
            array(
                'status' => rest_authorization_required_code(),
                'quote' => $quotes[$person],
                'author' => $person
            )
        );
    }

    return $result;

});
