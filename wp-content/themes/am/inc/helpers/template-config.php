<?php
/**
 * Theme Setup
 *
 * @package ThreeFiveOne
 */

/**
 * Create Pages
 *
 * @return void
 */
function threefiveone_do_pages_exist() {
    // Home
    if ( get_page_by_title( 'Home' ) == NULL ) create_page( 'Home' );
    // About
    if ( get_page_by_title( 'About' ) == NULL ) create_page( 'About' );
}
add_action('init', 'threefiveone_do_pages_exist');

function create_page( $name ) {
    $createPage = array(
        'post_title'    => $name,
        'post_content'  => '',
        'post_status'   => 'publish',
        'post_author'   => 1,
        'post_type'     => 'page',
        'post_name'     => $name
    );

    // Insert the post into the database
    wp_insert_post( $createPage );
}

/**
 * Create Page Templates
 *
 * @return void
 */
function threefiveone_create_custom_page_templates() {
    $pages = array(
        array(
            'page' => get_page_by_title('About'),
            'template' => 'page-about.php'
        ),
        array(
            'page' => get_page_by_title('Home'),
            'template' => 'page-home.php'
        ),
    );

    foreach ($pages as $page) {
        if ($page) {
            update_post_meta( $page['page']->ID, '_wp_page_template', $page['template'] );

            if ($page['template'] === 'page-home.php') {
                update_option( 'page_on_front', $page['page']->ID );
                update_option( 'show_on_front', 'page' );
            }
        }
    }
}
add_action('init', 'threefiveone_create_custom_page_templates');

/**
 * Create Custom Categories
 *
 * Commented out by default
 *
 * @return void
 */

function threefiveone_insert_categories() {
    wp_insert_term(
        'Stories',
        'category',
        array(
            'description' => 'Stories.',
            'slug' => 'stories'
        )
    );
    wp_insert_term(
        'Resources',
        'category',
        array(
            'description' => 'Resources.',
            'slug' => 'resources'
        )
    );
}
// add_action( 'init', 'threefiveone_insert_categories' );

/**
 * Set Permalink Structure
 *
 * @return void
 */
add_action( 'init', function() {
    global $wp_rewrite;
    $wp_rewrite->set_permalink_structure( '/%postname%/' );
} );

/**
 * Allow SVG Uploads
 *
 * @return string
 */
function threefiveone_cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'threefiveone_cc_mime_types');
