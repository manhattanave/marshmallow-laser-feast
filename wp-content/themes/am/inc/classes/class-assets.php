<?php
/**
 * Enqueue theme assets
 *
 * @package ThreeFiveOne
 */

namespace THREEFIVEONE_THEME\Inc;

use THREEFIVEONE_THEME\Inc\Traits\Singleton;

class Assets {
	use Singleton;

	protected function __construct() {
		// Load Class
		$this->setup_hooks();
	}

	protected function setup_hooks() {
		/**
		 * Actions
		 */
		add_action( 'init', [ $this, 'modify_jquery' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_styles' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ] );
	}

	public function register_styles() {
		// Register Styles
		wp_register_style( 'slick-css', THREEFIVEONE_BUILD_LIB_URI . '/css/slick.css', [], false, 'all' );
		wp_register_style( 'slick-theme-css', THREEFIVEONE_BUILD_LIB_URI . '/css/slick-theme.css', ['slick-css'], false, 'all' );
		wp_register_style( 'main-css', THREEFIVEONE_BUILD_CSS_URI . '/main.css', [], filemtime( THREEFIVEONE_BUILD_CSS_DIR_PATH . '/main.css' ), 'all' );

		// Enqueue Styles
		wp_enqueue_style( 'slick-css' );
		wp_enqueue_style( 'slick-theme-css' );
		wp_enqueue_style( 'main-css' );
	}

	public function register_scripts() {
		// Register Scripts
		wp_register_script( 'main-js', THREEFIVEONE_BUILD_JS_URI . '/main.js', ['jquery'], filemtime( THREEFIVEONE_BUILD_JS_DIR_PATH . '/main.js' ), true );

		// Enqueue Scripts
		wp_enqueue_script( 'main-js' );
	}

	// Making jQuery Google API
	public function modify_jquery() {
	    if (!is_admin() && !is_wplogin()) {
	        wp_deregister_script('jquery');
	        // wp_register_script('jquery', 'https://code.jquery.com/jquery-3.6.0.min.js', false, '3.6.0');
	        // wp_register_script('jquery', THREEFIVEONE_DIR_URI . '/assets/node_modules/jquery/dist/jquery.min.js', false, '3.6.0');
	        wp_register_script('jquery', THREEFIVEONE_BUILD_LIB_URI . '/js/jquery.js', false, '3.6.0');
	        wp_enqueue_script('jquery');
	    }
	}

}
