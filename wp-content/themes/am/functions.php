<?php
/**
 * Theme Functions
 *
 * @package ThreeFiveOne
 */

/*
 _   _                    __ _
| |_| |__  _ __ ___  ___ / _(_)_   _____  ___  _ __   ___
| __| '_ \| '__/ _ \/ _ \ |_| \ \ / / _ \/ _ \| '_ \ / _ \
| |_| | | | | |  __/  __/  _| |\ V /  __/ (_) | | | |  __/
 \__|_| |_|_|  \___|\___|_| |_| \_/ \___|\___/|_| |_|\___|

*/

if ( ! defined( 'THREEFIVEONE_DIR_PATH' ) ) {
	define( 'THREEFIVEONE_DIR_PATH', untrailingslashit( get_template_directory() ) );
}

if ( ! defined( 'THREEFIVEONE_DIR_URI' ) ) {
	define( 'THREEFIVEONE_DIR_URI', untrailingslashit( get_template_directory_uri() ) );
}

if ( ! defined( 'THREEFIVEONE_BUILD_URI' ) ) {
	define( 'THREEFIVEONE_BUILD_URI', untrailingslashit( get_template_directory_uri() ) . '/assets/build' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_PATH' ) ) {
	define( 'THREEFIVEONE_BUILD_PATH', untrailingslashit( get_template_directory() ) . '/assets/build' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_JS_URI' ) ) {
	define( 'THREEFIVEONE_BUILD_JS_URI', untrailingslashit( get_template_directory_uri() ) . '/assets/build/js' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_JS_DIR_PATH' ) ) {
	define( 'THREEFIVEONE_BUILD_JS_DIR_PATH', untrailingslashit( get_template_directory() ) . '/assets/build/js' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_IMG_URI' ) ) {
	define( 'THREEFIVEONE_BUILD_IMG_URI', untrailingslashit( get_template_directory_uri() ) . '/assets/build/img' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_ICON_URI' ) ) {
	define( 'THREEFIVEONE_BUILD_ICON_URI', untrailingslashit( get_template_directory_uri() ) . '/assets/build/icons' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_CSS_URI' ) ) {
	define( 'THREEFIVEONE_BUILD_CSS_URI', untrailingslashit( get_template_directory_uri() ) . '/assets/build/css' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_CSS_DIR_PATH' ) ) {
	define( 'THREEFIVEONE_BUILD_CSS_DIR_PATH', untrailingslashit( get_template_directory() ) . '/assets/build/css' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_LIB_URI' ) ) {
	define( 'THREEFIVEONE_BUILD_LIB_URI', untrailingslashit( get_template_directory_uri() ) . '/assets/build/library' );
}

if ( ! defined( 'THREEFIVEONE_BUILD_LIB_PATH' ) ) {
	define( 'THREEFIVEONE_BUILD_LIB_PATH', untrailingslashit( get_template_directory() ) . '/assets/build/library' );
}

if ( ! defined( 'THREEFIVEONE_VERSION_NUMBER' ) ) {
	$json = get_template_directory() . "/assets/package.json";
	$the_version = json_decode( file_get_contents( $json ), true );
	$the_version = $the_version["version"];

	define("THREEFIVEONE_VERSION_NUMBER", $the_version);
}

require_once THREEFIVEONE_DIR_PATH . '/inc/helpers/autoloader.php';
require_once THREEFIVEONE_DIR_PATH . '/inc/helpers/disable-features.php';
require_once THREEFIVEONE_DIR_PATH . '/inc/helpers/template-tags.php';
require_once THREEFIVEONE_DIR_PATH . '/inc/helpers/template-config.php';
require_once THREEFIVEONE_DIR_PATH . '/inc/helpers/custom-functions.php';
require_once THREEFIVEONE_DIR_PATH . '/inc/custom-post-types/custom-post-types.php';
require_once THREEFIVEONE_DIR_PATH . '/inc/custom-post-types/custom-taxonomies.php';

function threefiveone_get_theme_instance() {
	\THREEFIVEONE_THEME\Inc\THREEFIVEONE_THEME::get_instance();
}

threefiveone_get_theme_instance();
