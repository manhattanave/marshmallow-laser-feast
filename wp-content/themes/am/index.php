<?php
/**
 * Main template file.
 *
 * @package ThreeFiveOne
 */

get_header();
?>

	<main id="main" class="site-main" role="main">
		<?php

		// Section One: Intro
		get_template_part( 'template-parts/home/home', 'splash' );

		// Section Two: Mission
		// get_template_part( 'template-parts/home/mission' );

		// Section Three: About
		// get_template_part( 'template-parts/home/about' );

		// Section Four: Features
		// get_template_part( 'template-parts/home/features' );

		// Section Five: WP Query Loop
		// get_template_part( 'template-parts/home/loop', 'query' );

		// Section Six: ACF Loop
		// get_template_part( 'template-parts/home/loop', 'acf' );

		// Section Seven: Instagram
		// get_template_part( 'template-parts/instagram' );

		// Section Eight: FAQ
		// get_template_part( 'template-parts/home/loop', 'faq' );

		// Modal
		// get_template_part( 'template-parts/modals/modal' );

		?>
	</main>

<?php
get_footer();
?>
