<?php
/**
 * Featured Projects template
 *
 * @package threefiveone
 */

?>

<section class="featured-projects">
	<div class="grid-container">
		<div class="grid-x grid-padding-x">
			<?php 
			$projects = get_field('featured_projects');
			$projects = !empty($projects) ? $projects : false;
			
			if ($projects):
				$i = 1;
				// print_r(3 % 3);
				foreach ($projects as $project):
					$orientation = ($i % 2 == 0 || $i % 3 == 0 || $i % 7 == 0) ? 'portrait' : 'landscape';
					$orientation = ($i % 4 == 0) ? 'landscape' : $orientation;
					$orientation_class = ($i % 2 == 0 || $i % 3 == 0 || $i % 7 == 0) ? 'featured-projects__column--portrait' : 'featured-projects__column--landscape';
					$orientation_class = ($i % 4 == 0) ? 'featured-projects__column--landscape' : $orientation_class;
					$project_images = get_field('project_images', $project->ID);
					$project_images = !empty($project_images) ? $project_images : false;
					$project_image_portrait = !empty($project_images['project_thumbnail_portrait']) ? $project_images['project_thumbnail_portrait']['url'] : am_get_image('mlf-fallback-portrait.jpg');
					$project_image_landscape = !empty($project_images['project_thumbnail_landscape']) ? $project_images['project_thumbnail_landscape']['url'] : am_get_image('mlf-fallback-landscape.jpg');
					$project_info = get_field('project_info', $project->ID);
					$project_title = !empty($project_info['project_name']) ? $project_info['project_name'] : get_the_title($project->ID);
					$project_category = !empty($project_info['project_category_text']) ? $project_info['project_category_text'] : '';
					$project_year = !empty($project_info['project_year']) ? $project_info['project_year'] : '';


					if ($orientation == 'landscape') {
						$project_image = $project_image_landscape;
					} else {
						$project_image = $project_image_portrait;						
					}
				 ?>
				<div class="cell medium-6 featured-projects__column <?php echo $orientation_class; ?>">
					<div class="featured-project">
						<a href="<?php echo get_the_permalink($project->ID); ?>">
							<img class="featured-project__image featured-project__image--<?php echo $orientation; ?>" src="<?php echo $project_image; ?>">
							<h2 class="featured-project__title"><?php echo $project_title; ?></h2>
							<span class="featured-project__category"><?php echo $project_category; ?></span>
							<span class="featured-project__year"><?php echo $project_year; ?></span>
						</a>
					</div>

				</div>
			<?php 
				$i++;
				endforeach;
			endif; ?>
		</div>
	</div>
</section>