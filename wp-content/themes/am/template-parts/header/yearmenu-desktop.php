<?php if (is_post_type_archive('exhibitions')): ?>

	<?php 
	$year_menu = array('2010','2011','2012','2013','2014','2015','2016','2017','2018','2019','2020','2021','2022');
	$year_menu = array_reverse($year_menu);
	$menu_class = \ThreeFiveOne_Theme\Inc\Menus::get_instance();
	 ?>
	 
	<?php
	// Project Category Menu
	$cat_menu_slug = 'threefiveone-projects-categories';
	$cat_menu_id = $menu_class->get_menu_id( $cat_menu_slug );
	$cat_menu = wp_get_nav_menu_items( $cat_menu_id );
	$cat_menu_title = wp_get_nav_menu_name( $cat_menu_slug );			
	?>	 
	<?php if (!empty($year_menu)): ?>
	<nav class="category-nav hide-for-small-only">
		<?php if ( ! empty( $year_menu ) && is_array( $year_menu ) ) : ?>
		<ul class="category-nav__topics">
			<li class="category-nav__topic isotop-filters isotop-filters__active" data-filter="*">All</li>
			<?php foreach ( $year_menu as $menu_item ): ?>				
			<li class="category-nav__topic isotop-filters" data-filter=".<?php echo $menu_item; ?>"><?php echo $menu_item; ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
		<?php if ( ! empty( $cat_menu ) && is_array( $cat_menu ) ) : ?>
		<ul class="category-nav__cats">
			<?php foreach ( $cat_menu as $menu_item ): ?>
			<?php 
			$url = esc_url( $menu_item->url );
			$filter_name = basename(parse_url($url, PHP_URL_PATH));
			 ?>						
			<li class="category-nav__topic isotop-filters" data-filter=".<?php echo $filter_name; ?>"><?php echo esc_html( $menu_item->title ); ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>	
	</nav>
	<?php endif; ?>

<?php endif; ?>