<?php if (is_post_type_archive('projects')): ?>

	<?php
	$menu_class = \ThreeFiveOne_Theme\Inc\Menus::get_instance();

	// Project Topic Menu
	$topic_menu_slug = 'threefiveone-projects-topics';
	$topic_menu_id = $menu_class->get_menu_id( $topic_menu_slug );
	$topic_menu = wp_get_nav_menu_items( $topic_menu_id );
	$topic_menu_title = wp_get_nav_menu_name( $topic_menu_slug );

	// Project Category Menu
	$cat_menu_slug = 'threefiveone-projects-categories';
	$cat_menu_id = $menu_class->get_menu_id( $cat_menu_slug );
	$cat_menu = wp_get_nav_menu_items( $cat_menu_id );
	$cat_menu_title = wp_get_nav_menu_name( $cat_menu_slug );			
	?>

	<?php if (!empty($topic_menu)): ?>
	<nav class="category-nav hide-for-small-only">
		<?php if ( ! empty( $topic_menu ) && is_array( $topic_menu ) ) : ?>
		<ul class="category-nav__topics">
			<li class="category-nav__topic isotop-filters isotop-filters__active" data-filter="*">All</li>
			<?php foreach ( $topic_menu as $menu_item ): ?>
				<?php 
				$url = esc_url( $menu_item->url );
				$filter_name = basename(parse_url($url, PHP_URL_PATH));
				 ?>
			<li class="category-nav__topic isotop-filters" data-filter=".<?php echo $filter_name; ?>"><?php echo esc_html( $menu_item->title ); ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>

		<?php if ( ! empty( $cat_menu ) && is_array( $cat_menu ) ) : ?>
		<ul class="category-nav__cats">
			<?php foreach ( $cat_menu as $menu_item ): ?>
			<?php 
			$url = esc_url( $menu_item->url );
			$filter_name = basename(parse_url($url, PHP_URL_PATH));
			 ?>						
			<li class="category-nav__topic isotop-filters" data-filter=".<?php echo $filter_name; ?>"><?php echo esc_html( $menu_item->title ); ?></li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</nav>
	<?php endif; ?>
<?php endif; ?>