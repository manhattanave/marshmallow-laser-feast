<?php
/**
 * Homepage: FAQ Accordion Loop with ACF Fields
 *
 * @package ThreeFiveOne
 */

$faq_posts = get_field('faq_accordion');
$faq_loop = !empty($faq_posts) ? $faq_posts : false;

if ($faq_loop):
?>

<section class="section section-home section-home__faq">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell">
				<div class="accordions">
					<?php
					foreach ($faq_loop as $post):
						get_template_part( 'template-parts/loop/faq' );
					endforeach;

					wp_reset_postdata();
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<?php endif; ?>
