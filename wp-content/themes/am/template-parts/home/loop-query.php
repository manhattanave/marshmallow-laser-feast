<?php
/**
 * Homepage Section Five: WP_Query Loop
 *
 * @package ThreeFiveOne
 */

$args = array(
	'post_type' => 'post',
	'posts_per_page' => 4
);
$loop = new WP_Query( $args );
?>

<section class="section section-home">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell">

				<?php
				if ( $loop->have_posts() ) {
					$count = 0;
					while ( $loop->have_posts() ) : $loop->the_post();
						// Article
						get_template_part( 'template-parts/loop/article' );

						$count++;
					endwhile;
				}
				wp_reset_postdata();
				?>

			</div>
		</div>
	</div>
</section>
