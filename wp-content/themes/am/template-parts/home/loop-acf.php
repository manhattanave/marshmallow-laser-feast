<?php
/**
 * Homepage: Loop with ACF Fields
 *
 * @package ThreeFiveOne
 */

$acf_posts = get_field('featured_content');
$acf_loop = !empty($acf_posts) ? $acf_posts : false;

if ($acf_loop):
?>

<section class="section section-home section-home__content">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell">

				<?php
				// $count = count($acf_loop);
				// $count = ($count <= 4 ? $count : 4);
				// $class = 'cell small-12 medium-' . (12 / intval($count)) . ' section-home__content--card';

				foreach ($acf_loop as $post):

					setup_postdata($post);

					// Article
					get_template_part( 'template-parts/loop/article' );
				endforeach;

				wp_reset_postdata();
				?>

			</div>
		</div>
	</div>
</section>

<?php endif; ?>
