<?php
/**
 * Homepage: Intro Slider
 *
 * @package ThreeFiveOne
 */

$slides = get_field('intro_slider');

if (!empty($slides)):
?>

<section class="section section-home section-home__intro">
	<div class="section-home__intro--slider">
		<?php
		// Loop Over Slides
		foreach ($slides as $s):
			$background = !empty($s['intro_background_image']) ? $s['intro_background_image']['url'] : false;
			$background = ( $background ? ' style="background-image:url(' . $background . ');"' : '' );
			$mobile_background = !empty($s['intro_background_mobile_image']) ? $s['intro_background_mobile_image']['url'] : false;
			$mobile_background = ( $mobile_background ? ' style="background-image:url(' . $mobile_background . ');"' : '' );
			$intro_title = !empty($s['intro_title']) ? esc_html($s['intro_title']) : '';
			$intro_lead_paragraph = !empty($s['intro_lead_paragraph']) ? $s['intro_lead_paragraph'] : false;
			$intro_button_text = !empty($s['intro_button_text']) ? esc_html($s['intro_button_text']) : false;
			$intro_button_link = !empty($s['intro_button_link']) ? $s['intro_button_link'] : false;
			$intro_featured_image = !empty($s['intro_featured_image']) ? $s['intro_featured_image']['url'] : false;
		?>

		<div class="section-home__intro--background"<?php echo $background; ?>>
			<div class="section-home__intro--mobile-background"<?php echo $mobile_background; ?>></div>
			<div class="section-home__intro--overlay"></div>
			<div class="grid-container">
				<div class="grid-x grid-padding-x grid-padding-y align-middle section-home__intro--content">
					<div class="cell small-12 medium-8 large-6 medium-offset-2 large-offset-1">
						<h1 class="section-home__intro--title"><?php echo $intro_title; ?></h1>
						<?php if ($intro_lead_paragraph): ?>
							<div class="section-home__intro--lead">
								<?php echo $intro_lead_paragraph; ?>
							</div>
						<?php endif; ?>
						<?php
						if ($intro_button_text && $intro_button_link):
							am_button($intro_button_link, $intro_button_text);
						endif;
						?>
					</div>
					<?php if ($intro_featured_image): ?>
						<div class="cell small-12 medium-auto">
							<img src="<?php echo $intro_featured_image; ?>">
						</div>
					<?php endif; ?>
				</div>
			</div>
		</div>

		<?php
		endforeach;
		?>
	</div>
</section>

<?php endif; ?>
