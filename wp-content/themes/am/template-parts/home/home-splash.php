<?php
/**
 * Homepage: Intro Splash
 *
 * @package ThreeFiveOne
 */ 

$projects = get_field('splash_exhibitions');
$projects = (!empty($projects)) ? $projects : false; ?>

<section class="section section-home home-splash">
	<div class="grid-container full">
		<div class="grid-x">
			<div class="cell medium-6">
				<?php
				if ($projects) :
					foreach ($projects as $project) :
						$exhibition_info = get_field('exhibition_info', $project->ID);
						$exhibition_info = !empty($exhibition_info) ? $exhibition_info : false;

						$exhibition_name = !empty($exhibition_info['exhibition_name']) ? $exhibition_info['exhibition_name'] : get_the_title($project->ID);
						$exhibition_venue = !empty($exhibition_info['exhibition_venue']) ? $exhibition_info['exhibition_venue'] : '';
						$exhibition_location = !empty($exhibition_info['exhibition_location']) ? $exhibition_info['exhibition_location'] : '';
						$exhibition_date = !empty($exhibition_info['exhibition_date']) ? $exhibition_info['exhibition_date'] : '';
						$exhibition_end_date = !empty($exhibition_info['exhibition_end_date']) ? $exhibition_info['exhibition_end_date'] : '';
						$exhibition_time = !empty($exhibition_info['exhibition_time']) ? $exhibition_info['exhibition_time'] : '';
						$exhibition_timezone = !empty($exhibition_info['exhibition_timezone']) ? $exhibition_info['exhibition_timezone'] : '';
						$exhibition_url = get_field('exhibition_link', $project->ID);
					 ?>
					<div class="home-splash__project">
						<h2 class="home-splash__project-name"><?php echo $exhibition_name; ?></h2>
						<span class="home-splash__project-date"><?php echo $exhibition_date; ?> - <?php echo $exhibition_end_date; ?></span>
						<span class="home-splash__project-location"><?php echo $exhibition_venue; ?> <?php echo $exhibition_location; ?></span>
						<span class="home-splash__project-cta">Tickets are available <a href="<?php echo $exhibition_url ?>" target="_blank">here</a></span>
					</div>
				<?php endforeach;
				endif; ?>
			</div>


		</div>
	</div>
	<!-- <div class="home-splash__video" style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/611830886?h=f6a978dfa0&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="web_banner_sequence.mp4"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script> -->
    <video class="home-splash__video" autoplay loop muted playsinline src="https://player.vimeo.com/progressive_redirect/playback/611830886/rendition/1080p?loc=external&signature=63224043ab46626c6142be20741360375c80f0c639de8fad7b13d42a9939b879" type="video/mp4"></video>
</section>
