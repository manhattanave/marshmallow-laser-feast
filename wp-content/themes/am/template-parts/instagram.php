<?php
/**
 * Instagram
 *
 * @package ThreeFiveOne
 */
?>

<section class="section section-instagram">
	<div class="grid-container">
		<div class="grid-x align-center">
			<div class="cell small-10 medium-5 large-6">
				<h2>@UsernameGoesHere</h2>
			</div>
		</div>
		<div class="grid-x">
			<div class="cell">
				<div id="instagramfeed"></div>
			</div>
		</div>
	</div>
</section>
