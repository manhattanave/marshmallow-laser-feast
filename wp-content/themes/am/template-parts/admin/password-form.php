<?php
/**
 * Password Form
 *
 * @package ThreeFiveOne
 */
?>

<main role="main" aria-label="Content">
	<section class="section ">
		<div class="grid-container">
			<div class="grid-x grid-padding-x grid-padding-y align-middle align-center">
				<div class="cell">
					<h1 class="section-archive__intro--title"><?php the_title(); ?></h1>
					<div class="section-archive__intro--lead">
						<?php
						// we will show password form here
						echo threefiveone_password_form();
						?>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
