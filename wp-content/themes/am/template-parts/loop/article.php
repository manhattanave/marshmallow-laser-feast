<?php
/**
 * Standard Loop Article
 *
 * @package ThreeFiveOne
 */

$title = get_the_title();
$image = has_post_thumbnail() ? get_the_post_thumbnail_url() : false;
$excerpt = truncate_by_word( get_the_excerpt(), 240, '' );
$url = get_the_permalink();
?>

<article class="article animate__fade-in">
	<div class="article__featured-image">
		<a href="<?php echo $url; ?>" target="_blank">
		<?php
		if ( $image ) {
			echo '<div class="twobyone" style="background-image:url(' . esc_html( $image ) . ');"></div>';
		} else {
			echo '<div class="twobyone" style="background-image:url(' . am_get_image('fallback.png') . ');"></div>';
		}
		?>
		</a>
	</div>

	<div class="article__title-wrapper">
		<a href="<?php echo $url; ?>" target="_blank">
			<h3 class="article__title"><?php echo $title; ?></h3>
		</a>
		<p class="article__paragraph"><?php echo $excerpt; ?></p>
	</div>
</article>
