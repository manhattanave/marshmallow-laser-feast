<?php
/**
 * FAQ Loop Article
 *
 * @package ThreeFiveOne
 */

$question = !empty($post['faq_question']) ? sanitize_text_field($post['faq_question']) : false;
$answer = !empty($post['faq_answer']) ? $post['faq_answer'] : false;

if ($question && $answer):
?>

<div class="accordion animate__fade-in">
	<div class="accordion__question">
		<h4><?php echo $question; ?></h4>
		<span class="accordion__question--icon"></span>
	</div>
	<div class="accordion__answer">
		<div class="accordion__answer--inner">
			<?php echo $answer; ?>
		</div>
	</div>
</div>

<?php
endif;
