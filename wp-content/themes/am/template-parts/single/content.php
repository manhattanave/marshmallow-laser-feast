<?php
/**
 * Content template
 *
 * @package threefiveone
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single'); ?>>

	<div class="grid-container">
		<div class="grid-x grid-padding-x grid-padding-y align-center">
			<div class="cell small-12 medium-10">

				<?php
				$image = ( has_post_thumbnail() ? get_the_post_thumbnail_url() : false );
				if ( $image ) {
					echo '<div class="twobyone" style="background-image:url(' . esc_html( $image ) . ');"></div>';
				} else {
					echo '<div class="twobyone" style="background-image:url(' . am_get_image('fallback.png') . ');"></div>';
				}
				?>

				<h1 class="single__title">
					<?php the_title(); ?>
				</h1>

				<?php if (has_excerpt()): ?>
					<div class="single__lead-paragraph">
						<?php the_excerpt(); ?>
					</div>
				<?php endif; ?>

				<div class="single__meta">
					<span class="single__meta--date">
						<?php the_date('m/d/Y', '', '', true); ?>
					</span>
				</div>

				<div class="single__content">
					<?php the_content(); // Dynamic Content. ?>
				</div>

			</div>
		</div>
	</div>

</article>
