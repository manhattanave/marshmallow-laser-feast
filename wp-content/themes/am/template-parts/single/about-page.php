<?php
/**
 * Content Page template
 *
 * @package threefiveone
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single single-about'); ?>>

	<div class="grid-container full">
		<div class="grid-x grid-padding-x">
			<div class="cell small-12 medium-6">

				<h1 class="single__title">
					<?php the_title(); ?>
				</h1>

				<div class="single__content">
					<?php the_content(); // Dynamic Content. ?>
				</div>

				<?php $contacts = get_field('about_contact');
				$contacts = !empty($contacts) ? $contacts : false;

				if ($contacts) :
				 ?>
				<h2 class="about__title">Contact</h2>
				<ul class="about__contacts">				
					<?php foreach ($contacts as $contact): ?>
						<li class="about__contact">
							<span><?php echo $contact['contact_label']; ?> | </span><span><?php echo $contact['contact_year']; ?></span>
						</li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>


				<?php $exhibitions = get_field('about_exhibitions');
				$exhibitions = !empty($exhibitions) ? $exhibitions : false;

				if ($exhibitions) :
				 ?>
				<h2 class="about__title">Exhibitions</h2>
				<ul class="about__contacts">				
					<?php foreach ($exhibitions as $exhibition):
						$exhibition_info = get_field('exhibition_info', $exhibition->ID);
						$exhibition_info = !empty($exhibition_info) ? $exhibition_info : false;
						$exhibition_name = !empty($exhibition_info['exhibition_name']) ? $exhibition_info['exhibition_name'] : '';
						$exhibition_location = !empty($exhibition_info['exhibition_location']) ? $exhibition_info['exhibition_location'] : '';
						$exhibition_venue = !empty($exhibition_info['exhibition_venue']) ? $exhibition_info['exhibition_venue'] : '';
						$exhibition_date = !empty($exhibition_info['exhibition_date']) ? $exhibition_info['exhibition_date'] : '';
						$exhibition_link = get_field('exhibition_link', $exhibition->ID);
					 ?>
						<li class="about__contact">
							<a href="<?php echo $exhibition_link; ?>" target="_blank">
								<?php echo $exhibition_name ?> - <?php echo $exhibition_venue; ?> - <?php echo $exhibition_date; ?> | <?php echo $exhibition_location; ?>
							</a>
						</li>
					<?php endforeach; ?>
				</ul>
				<?php endif; ?>				

			</div>
		</div>
	</div>

</article>
