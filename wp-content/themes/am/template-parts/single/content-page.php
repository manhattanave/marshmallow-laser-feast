<?php
/**
 * Content Page template
 *
 * @package threefiveone
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class('single'); ?>>

	<div class="grid-container">
		<div class="grid-x grid-padding-x grid-padding-y align-center">
			<div class="cell small-12 medium-10">

				<h1 class="single__title">
					<?php the_title(); ?>
				</h1>

				<?php if (has_excerpt()): ?>
					<div class="single__lead-paragraph">
						<?php the_excerpt(); ?>
					</div>
				<?php endif; ?>

				<div class="single__meta">
					<span class="single__meta--date">
						<?php the_date('m/d/Y', '', '', true); ?>
					</span>
				</div>

				<div class="single__content">
					<?php the_content(); // Dynamic Content. ?>
				</div>

				<div class="single__pagination">
					<?php
					wp_link_pages(
						[
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'threefiveone' ),
							'after'  => '</div>',
						]
					);
					?>
				</div>

			</div>
		</div>
	</div>

</article>
