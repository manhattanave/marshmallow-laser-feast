<?php
/**
 * The template part for displaying a message that posts cannot be found.
 *
 * @package threefiveone
 */

?>

<section class="no-result not-found">
	<div class="grid-container">
		<div class="grid-x">
			<div class="cell">
				<header class="page-header">
					<h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'threefiveone' ); ?></h1>
				</header>

				<div class="page-content">
					<?php
						if ( is_search() ) {
							?>
							<p><?php esc_html_e( 'Sorry but nothing matched your search item. Please try again with some different keywords', 'threefiveone'  ); ?></p>
							<?php
							get_search_form();
						} else {
							?>
							<p><?php esc_html_e( 'It seems that we cannot find what you are looking for . Perhaps search can help', 'threefiveone'  ); ?></p>
							<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
</section>
