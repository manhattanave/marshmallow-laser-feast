<?php
/**
 * Content template
 *
 * @package threefiveone
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class('single'); ?>>
	<div class="single__featured-image">
		<?php
		$image = ( has_post_thumbnail() ? get_the_post_thumbnail_url() : false );
		if ( $image ) {
			echo '<div class="twobyone" style="background-image:url(' . esc_html( $image ) . ');"></div>';
		} else {
			echo '<div class="twobyone" style="background-image:url(' . am_get_image('fallback.png') . ');"></div>';
		}
		?>
		<h1 class="single__title">
			<?php the_title(); ?>
		</h1>
		<?php $subnav = get_field('project_subnav');
		$subnav = !empty($subnav) ? $subnav : false;
		 ?>

		<?php if ($subnav): ?>
		<div class="single__subnav">
			<?php foreach ($subnav as $item) :
				$subnav_anchor = !empty($item['subnav_anchor']) ? $item['subnav_anchor'] : '';
				$subnav_label = !empty($item['subnav_label']) ? $item['subnav_label'] : '';
			 ?>
			<a href="#<?php echo $subnav_anchor; ?>" class="single__subnav-item"><?php echo $subnav_label; ?></a>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>
	<div class="single__main-content grid-container">
		<div class="grid-x grid-padding-x grid-padding-y align-center">
			<div class="cell small-12 medium-12">

				<?php if (has_excerpt()): ?>
					<div class="single__lead-paragraph">
						<?php the_excerpt(); ?>
					</div>
				<?php endif; ?>

<!-- 				<div class="single__meta">
					<span class="single__meta--date">
						<?php the_date('m/d/Y', '', '', true); ?>
					</span>
				</div> -->

				<div class="single__content">
					<?php the_content(); // Dynamic Content. ?>
				</div>
			</div>
		</div>

		<?php 
		$credits = get_field('project_credits');
		$credits = !empty($credits) ? $credits : false;
		if ($credits): ?>
		<div id="credits" class="grid-x grid-padding-x grid-padding-y single__credits">
			<?php foreach ($credits as $credit):
				$credit_heading = !empty($credit['project_credit_heading']) ? $credit['project_credit_heading'] : '';
				$credit_text = !empty($credit['project_credit_text']) ? $credit['project_credit_text'] : '';
			 ?>
			<div class="cell small-12 medium-6">		
				<h2 class="single__credit-heading"><?php echo $credit_heading; ?></h2>
				<?php echo $credit_text; ?>
			</div>
			<?php endforeach; ?>
		</div>
		<?php endif; ?>
	</div>

</article>
