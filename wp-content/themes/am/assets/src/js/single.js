// Styles
import "../sass/single.scss";

/*
     _             _
 ___(_)_ __   __ _| | ___
/ __| | '_ \ / _` | |/ _ \
\__ \ | | | | (_| | |  __/
|___/_|_| |_|\__, |_|\___|
             |___/

*/

( function ( root, $ ) {
	"use strict";

	$( function () {
		const threefiveone = {
			touch:
				"ontouchstart" in document.documentElement ? "touchstart" : "click",

			/**
			 * Init
			 */
			init: function () {
				//
			},
		};

		/**
		 * Fire Up
		 */
		threefiveone.init();

		// $(window).on("beforeunload", function() {});

		// $(window).resize(function() {});
	} );
} )( this, jQuery );
