// Primary Styles
import "../sass/fonts.scss";
import "../sass/header.scss";
import "../sass/main.scss";
import "../sass/home.scss";
import "../sass/single.scss";
import "../sass/page.scss";

/* eslint-disable no-unused-vars */

// FontFaceObserver
const FontFaceObserver = require( "fontfaceobserver" );

// WebFontLoader
const WebFont = require( "webfontloader" );

// Magnific Popup
const magnificPopup = require( "magnific-popup" );

// Slick
const Slick = require( "slick-carousel" );

// Match Height
const matchHeight = require( "jquery-match-height" );

// InstagramFeed
const InstagramFeed = require( '../library/js/instagramfeed.js' );

// Green Sock
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger.js";
gsap.registerPlugin( ScrollTrigger );

// Isotope
import Isotope from "isotope-layout/js/isotope";

// ImagesLoads
import ImagesLoaded from "imagesloaded";

/* eslint-enable no-unused-vars */

/*
								 _
 _ __ ___   __ _(_)_ __
| '_ ` _ \ / _` | | '_ \
| | | | | | (_| | | | | |
|_| |_| |_|\__,_|_|_| |_|

*/

( function( root, $ ) {
	"use strict";

	$( function() {
		jQuery.fn.load = function( callback ) {
			$( window ).on( "load", callback );
		};

		const threefiveone = {
			touch: "ontouchstart" in document.documentElement ? "touchstart" : "click",

			/**
			 * Init
			 */
			init: function() {
				this.fonts();
				// this.magnific_image();
				// this.magnific_video();
				this.magnific_modal();
				this.slider();
				this.match_height();
				this.nav_toggle();
				this.console_log();
				this.instagramfeed();
				this.faq();
				this.greensock();
				this.isotope();
				this.smooth_scroll();
				this.project_content_sticky();
				// this.sticky_nav();
			},

			/**
			 * Fonts
			 */
			fonts: function() {
				WebFont.load( {
					// google: {
					// 	families: [ "DM Sans:400,800" ],
					// },
					custom: {
					    families: ['Basis Grotesque Pro Off White']
					},
					timeout: 1000,
					classes: false,
					events: false,
					text: "abcdefghijklmnopqrstuvwxyz!@$%&*()[]{}=-_+,.`/'\"",
					/**
					 * Store Fonts in Session
					 */
					active: function() {
						sessionStorage.fonts = true;
					},
				} );

				// Check for Font
				const font_regular = new FontFaceObserver( 'Basis Grotesque Pro Off White', {
					weight: 400,
				} );
				const font_bold = new FontFaceObserver( 'Basis Grotesque Pro Off White', {
					weight: 800,
				} );
				Promise.all( [ font_regular.load(), font_bold.load() ] ).then( function() {
					$( "html" ).addClass( "font-active" );
				} );

				// Fallback
				setTimeout( function() {
					if ( !$( "html" ).hasClass( "font-active" ) ) {
						$( "html" ).addClass( "font-active" );
					}
				}, 1000 );
			},

			/**
			 * Magnific Images
			 */
			magnific_image: function() {
				$( ".image__popup" ).magnificPopup( {
					type: "image",
					mainClass: "mfp-with-zoom",
					zoom: {
						enabled: true,
						duration: 300, // duration of the effect, in milliseconds
						easing: "ease-in-out", // CSS transition easing function
						/**
						 * Get Image From Opener
						 *
						 * @param {object} openerElement Container with Image
						 * @return {object} The Image.
						 */
						opener: function( openerElement ) {
							if ( openerElement.is( "img" ) ) {
								return openerElement;
							} else {
								return openerElement.find( "img" );
							}
						},
					},
					gallery: {
						enabled: true,
					},
					closeBtnInside: false,
					closeOnContentClick: true,
				} );
			},

			/**
			 * Magnific Video
			 */
			magnific_video: function() {
				$( ".youtube" ).magnificPopup( {
					items: {
						src: "https://www.youtube.com/watch?v=###",
					},
					type: "iframe",
					iframe: {
						markup: "<div class='mfp-iframe-scaler'>" +
							"<div class='mfp-close'></div>" +
							"<iframe class='mfp-iframe' frameborder='0' allowfullscreen></iframe>" +
							"</div>",
						patterns: {
							youtube: {
								index: "youtube.com/",
								id: "v=",
								src: "//www.youtube.com/embed/%id%?autoplay=1",
							},
							vimeo: {
								index: "vimeo.com/",
								id: "/",
								src: "//player.vimeo.com/video/%id%?autoplay=1",
							},
						},
						srcAction: "iframe_src",
					},
				} );
			},

			/**
			 * Magnific Modal
			 */
			magnific_modal: function() {
				// Inline popups
				$( "#inline-popups" ).magnificPopup( {
					delegate: "a",
					removalDelay: 500, //delay removal by X to allow out-animation
					callbacks: {
						/**
						 * Runs before open
						 */
						beforeOpen: function() {
							this.st.mainClass = this.st.el.attr( "data-effect" );
						}
					},
					midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
				} );

				// Image popups
				$( "#image-popups" ).magnificPopup( {
					delegate: "a",
					type: "image",
					removalDelay: 500, //delay removal by X to allow out-animation
					callbacks: {
						/**
						 * Runs before open
						 */
						beforeOpen: function() {
							// just a hack that adds mfp-anim class to markup
							this.st.image.markup = this.st.image.markup.replace( "mfp-figure", "mfp-figure mfp-with-anim" );
							this.st.mainClass = this.st.el.attr( "data-effect" );
						}
					},
					closeOnContentClick: true,
					midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
				} );

				// Hinge effect popup
				$( "a.hinge" ).magnificPopup( {
					mainClass: "mfp-with-fade",
					removalDelay: 1000, //delay removal by X to allow out-animation
					callbacks: {
						/**
						 * Runs before close
						 */
						beforeClose: function() {
							this.content.addClass( "hinge" );
						},
						/**
						 * Runs on close
						 */
						close: function() {
							this.content.removeClass( "hinge" );
						}
					},
					midClick: true
				} );

			},

			/**
			 * Slick Slider
			 */
			slider: function() {
				// Homepage Intro Slider
				$( ".section-home__intro--slider" ).slick( {
					dots: true,
					arrows: false,
					infinite: true,
					speed: 444,
					slidesToShow: 1,
					slidesToScroll: 1,
					autoplay: true,
					autoplaySpeed: 6000,
				} );

				$( ".slider" ).slick( {
					centerMode: true,
					// centerPadding: "60px",
					dots: true,
					arrows: false,
					infinite: true,
					speed: 444,
					slidesToShow: 5,
					slidesToScroll: 3,
					autoplay: true,
					autoplaySpeed: 2222,
					responsive: [
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 4,
								slidesToScroll: 4,
							},
						},
						{
							breakpoint: 768,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3,
							},
						},
						{
							breakpoint: 600,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2,
							},
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1,
							},
						},
					],
				} );
			},

			/**
			 * Smooth Scroll
			 */
			smooth_scroll: function() {
				$( "a[href*='#']" ).each( function() {
					$( this ).on( threefiveone.touch, function() {
						const target = this.hash;
						$( "html, body" ).animate( {
							scrollTop: $( target ).offset().top - $( ".header" ).height(),
						}, 1000 );
					} );
				} );
			},


			/**
			 * Project content sticky
			 */
			project_content_sticky: function() {
				if ( $( '.single__main-content' ).length ) {
					ScrollTrigger.create( {
						trigger: ".single__main-content",
						markers: false,
						start: "top center",
						// end: "top top",
						  onEnter: () => {
						  	// console.log('xyz')
						  	$( '.single__subnav' ).addClass( 'active' );
						  },
						  onLeaveBack: () => {
						  	// console.log('abc')
						  	$( '.single__subnav' ).removeClass( 'active' );
						  },
			    } )
				}
			},

			/**
			 * Nav Toggle
			 */
			nav_toggle: function() {
				$( document ).on( threefiveone.touch, "#mobile-menu-button", function( e ) {
					e.preventDefault();
					$( this ).toggleClass( "anim-menu-btn--state-b" );
					$( ".nav" ).toggleClass( "active" );
					$( "body" ).toggleClass( "lock" );
				} );
			},

			/**
			 * jQuery Match Height
			 */
			match_height: function() {
				const options = {
					byRow: true,
					property: "height",
					target: null,
					remove: false,
				};
				$( ".util__equal" ).matchHeight( options );
			},

			/**
			 * Sticky Nav
			 */
			sticky_nav: function() {
				$( window ).scroll( function() {
					const scroll = $( window ).scrollTop(),
						header = $( ".header" ),
						div_top = $( "TOPSECTION" ).offset().top,
						height = $( "SECONDSECTION" ).height();

					if ( scroll > ( div_top + height ) / 4 ) {
						header.addClass( "background" );
					} else {
						header.removeClass( "background" );
					}
				} );
			},

			/**
			 * InstagramFeed
			 */
			instagramfeed: function() {
				if ( $( '#instagramfeed' ).length ) {
					/* eslint-disable no-bitwise */
					new InstagramFeed( {
						'username': 'markobajlovic',
						'container': document.getElementById( "instagramfeed" ),
						'display_profile': false,
						'display_biography': false,
						'display_gallery': true,
						'callback': null,
						'styling': true,
						'items': 8,
						'items_per_row': 4,
						'max_tries': 10,
						'margin': 1,
						'host': 'https://images' + ~~( Math.random() * 3333 ) + '-focus-opensocial.googleusercontent.com/gadgets/proxy?container=none&url=https://www.instagram.com/'
					} );
					/* eslint-disable no-bitwise */
				}
			},

			/**
			 * FAQ / Accordion
			 */
			faq: function() {
				if ( $( ".accordions" ).length ) {
					$( ".accordion" ).on( threefiveone.touch, function( e ) {
						e.preventDefault();
						$( this ).toggleClass( "open" );
					} );
				}
			},

			/**
			 * Isotope
			 */
			isotope: function() {
				if ( $( ".isotope" ).length ) {

					if ( $( 'body' ).hasClass( 'post-type-archive-projects' ) ) {

						$( "#projects-grid" ).imagesLoaded().done( function() {
							const iso = new Isotope( ".isotope", {
								  itemSelector: '.isotope-item',
								  percentPosition: true,
								  masonry: {
								    // use outer width of grid-sizer for columnWidth
								    columnWidth: '.isotope-item',
								    isFitWidth: true
								  }
							} );

							$( ".isotop-filters" ).on( threefiveone.touch, function( e ) {
								e.preventDefault();
								const options = { filter: $( this ).data( 'filter' ) };
								iso.arrange( options );
							} );

							// Click Nav Filters
							$( ".isotop-filters" ).on( threefiveone.touch, function( e ) {
								e.preventDefault();
								$( ".isotop-filters__active" ).removeClass( "isotop-filters__active" );
								$( this ).addClass( "isotop-filters__active" );
							} );
						} );

					} else if ( $( 'body' ).hasClass( 'post-type-archive-exhibitions' ) ) {

						$( "#exhibitions-grid" ).imagesLoaded().done( function() {
						// 	console.log( 'are we still goin' );
							const iso = new Isotope( ".isotope", {
								  itemSelector: '.isotope-item',
								  percentPosition: true,
								  masonry: {
								    // use outer width of grid-sizer for columnWidth
								    columnWidth: '.grid-sizer'
								  }
							} );

							$( ".isotop-filters" ).on( threefiveone.touch, function( e ) {
								e.preventDefault();
								const options = { filter: $( this ).data( 'filter' ) };
								iso.arrange( options );
							} );

							// Click Nav Filters
							$( ".isotop-filters" ).on( threefiveone.touch, function( e ) {
								e.preventDefault();
								$( ".isotop-filters__active" ).removeClass( "isotop-filters__active" );
								$( this ).addClass( "isotop-filters__active" );
							} );
						// });
						} );
					}
				}

			},

			/**
			 * Green Sock
			 */
			greensock: function() {
				const ele = ".animate__fade-in";
				if ( $( ele ).length ) {
					gsap.set( ele, { y: 50 } );

					ScrollTrigger.batch( ele, {
						once: true,
					  interval: 0.15,
					  batchMax: 3,
					  onEnter: batch => gsap.to( batch, {ease: "power2", opacity: 1, y: 0, stagger: {each: 0.15, grid: [1, 3]}, overwrite: true} ),
					  onEnterBack: batch => gsap.to( batch, {ease: "power2", opacity: 1, y: 0, stagger: 0.15, overwrite: true} ),
					  // onLeave: batch => gsap.set(batch, {opacity: 0, y: -100, overwrite: true}),
					  // onLeaveBack: batch => gsap.set(batch, {opacity: 0, y: 100, overwrite: true}),
					  start: "35px bottom",
					  end: "top top"
					} );

					ScrollTrigger.addEventListener( "refreshInit", () => gsap.set( ele, {y: 0} ) );
				}
			},

			/**
			 * Console Log Outputs
			 */
			console_log: function() {
				/* eslint-disable no-console, no-undef */
				console.log(
					"%cBuilt by 351 Studios",
					"background-color:#fff;color:#0A0A0B;padding:0.5em 1em;font-weight:900;line-height:1.5em;font-size:2em;"
				);
				console.log( "Build Version: " + THREEFIVEONEPROJECTVERSION );
				/* eslint-disable no-console, no-undef */
			},
		};

		/**
		 * Fire Up
		 */
		threefiveone.init();

		/**
		 * Fake Page Transitions, Animate Fade Out on Page Exit
		 */
		$( window ).on( "beforeunload", function() {
			$( "html" ).removeClass( "font-active" );
		} );

		// $(window).resize(function() {});
	} );
} )( this, jQuery );
