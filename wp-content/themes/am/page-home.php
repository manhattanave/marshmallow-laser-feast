<?php
/**
 * Template Name: Home
 *
 * @package threefiveone
 */

get_header();

if ( !post_password_required() ):
	// Homepage Content (Content in Index File)
	get_template_part('index');
	// Admin Edit
	get_template_part('template-parts/admin', 'edit');
else:
	// Password Form
	get_template_part('template-parts/password', 'form');
endif;

get_footer();
